//Create
db.users.insertMany([
	{
		firstName: "Tony",
		lastName: "Stark",
		email: "tonystark@gmail.com",
		password: "iloveu3000",
		isAdmin: false,
	},
	{
		firstName: "Steve",
		lastName: "Rogers",
		email: "steverogers@gmail.com",
		password: "peggyislife",
		isAdmin: false,
	},
	{
		firstName: "Thor",
		lastName: "Odinson",
		email: "thorodinson@gmail.com",
		password: "mjolnir",
		isAdmin: false,
	},
	{
		firstName: "Natasha",
		lastName: "Romanov",
		email: "natasharomanov@gmail.com",
		password: "avengersassemble",
		isAdmin: false,
	},
	{
		firstName: "Bruce",
		lastName: "Banner",
		email: "brucebanner@gmail.com",
		password: "brucesmash",
		isAdmin: false,
	},
	{
		firstName: "Clint",
		lastName: "Barton",
		email: "clintbarton@gmail.com",
		password: "quiverweaver",
		isAdmin: false,
	},
]);

db.courses.insertMany([
	{
		name: "Stopping Thanos 101",
		price: 999,
		isActive: false,
	},
	{
		name: "Avenger Assembling 101",
		price: 559,
		isActive: false,
	},
	{
		name: "Key to being Worthy",
		price: 600,
		isActive: false,
	},
]);

//Read
db.users.find({ isAdmin: false });

//Update
db.users.updateOne({}, { $set: { isAdmin: true } });
db.courses.updateOne({}, { $set: { isActive: true } });

//Delete
db.courses.deleteMany({ isActive: false });
